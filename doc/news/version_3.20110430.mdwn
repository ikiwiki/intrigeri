ikiwiki 3.20110430 released with [[!toggle text="these changes"]]
[[!toggleable text="""
   * meta: Allow adding javascript to pages. Only when htmlscrubber is
     disabled, naturally. (Thanks, Giuseppe Bilotta) Closes: #[623154](http://bugs.debian.org/623154)
   * comments: Add avatar picture of comment author, using Libravatar::URL
     when available. The avatar is looked up based on the user's openid,
     or email address. (Thanks, Francois Marier)
   * Recommend libgravatar-url-perl, which contains Libravatar::URL.
   * monotone: Implement rcs\_getmtime, and work around a problem with monotone
     0.48 that affects rcs\_getctime. (Thanks, Richard Levitte)
   * meta: Fix bug in loading of HTML::Entities that can break inline
     archive=yes (mostly masked by other plugins that load the module).
   * Be quiet about updating wrappers, except in verbose mode. (jmtd)
   * meta: Add FOAF support. Closes: #[623156](http://bugs.debian.org/623156) (Jonas Smedegaard)
   * Promote Crypt::SSLeay to Recommends; needed for https openid auth.
   * tag: Avoid autocreating multiple tag pages that vary only in
     capitalization. The first capitalization seen of a tag will be used
     for the tag page.
   * Fix yaml build dep. Closes: #[624712](http://bugs.debian.org/624712)"""]]