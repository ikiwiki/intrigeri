Ikiwiki should really survive being asked to work with a git branch that has no existing commits.

    mkdir iki-gittest
    cd iki-gittest
    GIT_DIR=barerepo.git git init
    git clone barerepo.git srcdir
    ikiwiki --rcs=git srcdir destdir

I've fixed this initial construction case, and, based on my testing, I've also fixed the post-update executing on a new master, and ikiwiki.cgi executing on a non-existent master cases.

Please commit so my users stop whining at me about having clean branches to push to, the big babies.

Summary: Change three scary loud failure cases related to empty branches into three mostly quiet success cases.

[[!tag patch]]

<pre>
diff --git a/IkiWiki/Plugin/git.pm b/IkiWiki/Plugin/git.pm
index cf7fbe9..e5bafcf 100644
--- a/IkiWiki/Plugin/git.pm
+++ b/IkiWiki/Plugin/git.pm
@@ -439,17 +439,21 @@ sub git_commit_info ($;$) {
 
 	my @opts;
 	push @opts, "--max-count=$num" if defined $num;
-
-	my @raw_lines = run_or_die('git', 'log', @opts,
-		'--pretty=raw', '--raw', '--abbrev=40', '--always', '-c',
-		'-r', $sha1, '--', '.');
-
+	my @raw_lines;
 	my @ci;
-	while (my $parsed = parse_diff_tree(\@raw_lines)) {
-		push @ci, $parsed;
-	}
+        
+	# Test to see if branch actually exists yet.
+	if (run_or_non('git', 'show-ref', '--quiet', '--verify', '--', 'refs/heads/' . $config{gitmaster_branch}) ) {
+		@raw_lines = run_or_die('git', 'log', @opts,
+			'--pretty=raw', '--raw', '--abbrev=40', '--always', '-c',
+			'-r', $sha1, '--', '.');
+
+		while (my $parsed = parse_diff_tree(\@raw_lines)) {
+			push @ci, $parsed;
+		}
 
-	warn "Cannot parse commit info for '$sha1' commit" if !@ci;
+		warn "Cannot parse commit info for '$sha1' commit" if !@ci;
+	};
 
 	return wantarray ? @ci : $ci[0];
 }
@@ -474,7 +478,10 @@ sub rcs_update () {
 	# Update working directory.
 
 	if (length $config{gitorigin_branch}) {
-		run_or_cry('git', 'pull', '--prune', $config{gitorigin_branch});
+		run_or_cry('git', 'fetch', '--prune', $config{gitorigin_branch});
+		if (run_or_non('git', 'show-ref', '--quiet', '--verify', '--', 'refs/remotes/' . $config{gitorigin_branch} . '/' . $config{gitmaster_branch}) ) {
+			run_or_cry('git', 'merge', $config{gitorigin_branch} . '/' . $config{gitmaster_branch});
+		}
 	}
 }
 
@@ -559,7 +566,7 @@ sub rcs_commit_helper (@) {
 	# So we should ignore its exit status (hence run_or_non).
 	if (run_or_non('git', 'commit', '-m', $params{message}, '-q', @opts)) {
 		if (length $config{gitorigin_branch}) {
-			run_or_cry('git', 'push', $config{gitorigin_branch});
+			run_or_cry('git', 'push', $config{gitorigin_branch}, $config{gitmaster_branch});
 		}
 	}
 	
</pre>
